import {
  BanknotesIcon,
  UserPlusIcon,
  UserIcon,
  ChartBarIcon,
  ClockIcon,
  TagIcon,
  ShoppingCartIcon
  
} from "@heroicons/react/24/solid";
import { BsTable } from "react-icons/bs";
import { MdHome } from "react-icons/md";
import { FaUser, FaPizzaSlice, FaShoppingBag } from "react-icons/fa";
import { RiSettings4Line } from "react-icons/ri";
import { FaDollarSign } from "react-icons/fa";
import { FaClipboardList } from "react-icons/fa";
import { FaChartBar, FaCog } from "react-icons/fa";
import { FaSignOutAlt } from "react-icons/fa";
import { TiThMenu } from "react-icons/ti";
// import { RiFruitLine } from 'react-icons/ri';

export const statisticsCardsData = [
  {
    color: "blue",
    icon: ClockIcon,
    title: "Todays Items",
    value: "$53k",
    footer: {
      color: "text-green-500",
      value: "+55%",
      label: "than last week",
    },
  },
  {
    color: "pink",
    icon: TagIcon,
    title: "Total Categories",
    value: "2,300",
    footer: {
      color: "text-green-500",
      value: "+3%",
      label: "than last month",
    },
  },
  {
    color: "green",
    icon: ShoppingCartIcon,
    title: "Best Selling Items",
    value: "3,462",
    footer: {
      color: "text-red-500",
      value: "-2%",
      label: "than yesterday",
    },
  },
  {
    color: "orange",
    icon: ChartBarIcon,
    title: "Total Sales",
    value: "$103,430",
    footer: {
      color: "text-green-500",
      value: "+5%",
      label: "than yesterday",
    },
  },
];

export default statisticsCardsData;
