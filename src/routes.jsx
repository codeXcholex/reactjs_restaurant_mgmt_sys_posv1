import {
  HomeIcon,
  UserCircleIcon,
  TableCellsIcon,
  BellIcon,
  ArrowRightOnRectangleIcon,
  UserPlusIcon,
} from "@heroicons/react/24/solid";
import { Home, Profile, Tables, Notifications } from "@/pages/dashboard";
import Dashboards from "./pages/dashboard/dashboards";
import Customers from "./pages/dashboard/customers";
import { SignIn, SignUp } from "@/pages/auth";
import { FaUsers,FaDollarSign } from 'react-icons/fa';
import { RiSettings4Line,RiRestaurant2Fill } from 'react-icons/ri';
import { FaChartLine } from 'react-icons/fa';
import { BiFoodMenu } from 'react-icons/bi';


import Cashier from "./pages/dashboard/cashier";
import Orders from "./pages/dashboard/orders";
import Setting from "./pages/dashboard/setting";
import Main_menu from "./pages/menuMart/main_menu";

const icon = {
  className: "w-5 h-5 text-inherit",
};

export const routes = [
  {
    layout: "dashboard",
    pages: [
      {
        icon:<HomeIcon {...icon}/>,
        name: "Home",
        path: "/home",
        element: <Home />,
      },
      {
        icon: <RiRestaurant2Fill {...icon} />,
        name: "Menu",
        path: "/menu",
        element: <Main_menu />,
      },
      {
        icon:<FaUsers {...icon}/>,
        name: "Customers",
        path: "/customer",
        element: <Customers />,
      },
      {
        icon: <TableCellsIcon {...icon} />,
        name: "Tables",
        path: "/table",
        element: <Tables />,
      },
      {
        icon: <FaDollarSign {...icon} />,
        name: "Cashier",
        path: "/cashier",
        element: <Cashier />,
      },
      {
        icon: <BiFoodMenu {...icon} />,
        name: "Orders",
        path: "/order",
        element: <Orders />,
      },

      {
        icon: <FaChartLine {...icon} />,
        name: "Reports",
        path: "/report",
        element: <Dashboards />,
      },
      {
        icon: <UserCircleIcon {...icon} />,
        name: "profile",
        path: "/profile",
        element: <Profile />,
      },
      {
        icon: <BellIcon {...icon} />,
        name: "notifactions",
        path: "/notifactions",
        element: <Notifications />,
      },
      {
        icon: <RiSettings4Line {...icon} />,
        name: "setting",
        path: "/setting",
        element: <Setting />,
      },
    ],
  },
  {
    title: "auth pages",
    layout: "auth",
    pages: [
      {
        icon: <ArrowRightOnRectangleIcon {...icon} />,
        name: "sign in",
        path: "/sign-in",
        element: <SignIn />,
      },
      {
        icon: <UserPlusIcon {...icon} />,
        name: "sign up",
        path: "/sign-up",
        element: <SignUp />,
      },
    ],
  },
];

export default routes;
