const sortFlowsHome =  [
    {
      _id:"A-Z",
      flow:"A-Z"
    },
    {
      _id:"Z-A",
      flow:"Z-A"
    },
    {
      _id:"H-L",
      flow:"H-L"
    },
    {
      _id:"L-H",
      flow:"L-H"
    },
    {
      _id:"Oldest",
      flow:"Older"
    },
    {
      _id:"Latest",
      flow:"Newer"
    },
  ]
  
  export default sortFlowsHome;
  