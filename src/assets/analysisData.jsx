import {
    BanknotesIcon,
    UserPlusIcon,
    UserIcon,
    ChartBarIcon,
    ClockIcon,
    TagIcon,
    ShoppingCartIcon
    
  } from "@heroicons/react/24/solid";
  import { BsTable } from "react-icons/bs";
  import { MdHome } from "react-icons/md";
  import { FaUser, FaPizzaSlice, FaShoppingBag } from "react-icons/fa";
  import { RiSettings4Line } from "react-icons/ri";
  import { FaDollarSign } from "react-icons/fa";
  import { FaClipboardList } from "react-icons/fa";
  import { FaChartBar, FaCog } from "react-icons/fa";
  import { FaSignOutAlt } from "react-icons/fa";
  import { TiThMenu } from "react-icons/ti";
  // import { RiFruitLine } from 'react-icons/ri';
  
  
  
 export function GetCartDynamicData(type,data){

  if(type=="Foods"){
    const statisticsCardsData = [
      {
        color: "blue",
        icon: ClockIcon,
        title: "Added Today",
        value: "+"+`${data.addedTodayFoods.length.toFixed(0)}`,
        footer: {
          color: "text-green-500",
          value: "+55%",
          label: "than last week",
        },
      },
      {
        color: "pink",
        icon: TagIcon,
        title: "Most Ordered Foods",
        value: `${data.mostOrderedFoods.length.toFixed(2)}`,
        footer: {
          color: "text-green-500",
          value: "+3%",
          label: "than last month",
        },
      },
      {
        color: "green",
        icon: ShoppingCartIcon,
        title: "*Items ordered",
        value: `${data.totalOrderCount.toFixed(0)}`,
        footer: {
          color: "text-red-500",
          value: "-2%",
          label: "than yesterday",  
        },
      },
      {
        color: "orange",
        icon: ChartBarIcon,
        title: "Total Listed Food",
        value: `${data.listedFoodCount.toFixed(0)}`,
        footer: {
          color: "text-green-500",
          value: "+5%",
          label: "than yesterday",
        },
      },
    ];
    return statisticsCardsData;
  }
  
    const statisticsCardsData = [
      {
        color: "blue",
        icon: ClockIcon,
        title: "Todays Cart",
        value: "+"+`${data.todaysCart.length}`,
        footer: {
          color: "text-green-500",
          value: "+55%",
          label: "than last week",
        },
      },
      {
        color: "pink",
        icon: TagIcon,
        title: "Todays Earnings",
        value: "$"+`${data.todaysEarnings.toFixed(2)}`,
        footer: {
          color: "text-green-500",
          value: "+3%",
          label: "than last month",
        },
      },
      {
        color: "green",
        icon: ShoppingCartIcon,
        title: "Today Item On Cart",
        value: `${data.todaysCartItemCount.length}`,
        footer: {
          color: "text-red-500",
          value: "-2%",
          label: "than yesterday",
        },
      },
      {
        color: "orange",
        icon: ChartBarIcon,
        title: "Total Carts",
        value: `${data.listedCartCount}`,
        footer: {
          color: "text-green-500",
          value: "+5%",
          label: "than yesterday",
        },
      },
    ];
  
    return statisticsCardsData;
  }
  

  
  
  
  