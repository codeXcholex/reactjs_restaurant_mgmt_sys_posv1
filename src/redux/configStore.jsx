import { createStore, applyMiddleware, combineReducers } from "redux";
import { Categories } from "./Categories/reducer";
import { Items } from "./Items/reducer";
import {Users} from "./Users/reducer"
import { AnalyzedFoodData } from "./AnalyzedFoods/reducer";
import { AnalyzedCartData } from "./AnalyzedCart/reducer";
import { Bills } from "./Orders/reducer";


import thunk from "redux-thunk";
import logger from "redux-logger";
import { composeWithDevTools } from "redux-devtools-extension";

export const ConfigStore = () => {
  const env = process.env.NODE_ENV || "development";
  const store = createStore(
    combineReducers({
      category: Categories,
      foods: Items,
      users: Users,
      analyzedCart:AnalyzedCartData,
      analyzedFood: AnalyzedFoodData,
      bills: Bills
    }),
    env === "development"
      ? composeWithDevTools(applyMiddleware(thunk, logger))
      : applyMiddleware(thunk)
  );
  return store;
};
