import * as ActionTypes from "./actionTypes";
import axios from "axios";

export const usersLoading = () => ({
  type: ActionTypes.USERS_LOADING,
});


export const getUsers = () => async (dispatch) => {
  try {
    dispatch(usersLoading());
    const BACKEND_BASE_URL = "https://surfit.onrender.com";
    const { data } = await axios.get(BACKEND_BASE_URL + "/api/pos");
    data.success && dispatch({ type: ActionTypes.GET_USERS, payload: data.users })
  } catch (error) {
    console.error("Error:", error);
  }
};

