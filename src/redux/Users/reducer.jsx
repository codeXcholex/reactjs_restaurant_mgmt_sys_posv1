import * as ActionTypes from "./actionTypes"

const initialState = {
  Users: [],
  isLoading: true,
  success: null,
};

export const Users = (state = initialState, action) => {
  switch (action.type) {
    case ActionTypes.GET_USERS:
      const datas = {
        ...state,
        Users: action.payload,
        isLoading: false,
        success: true
      };
      return datas;
    default:
      return state;
  }

};
