import * as ActionTypes from "./actionTypes"

const initialCartAnalysisState = {
  isLoading: true,
  success: null,
  todaysCart:[],
  todaysEarnings:0,
  todaysCartItemCount:[],
  listedCartCount:0
};

export const AnalyzedCartData = (state = initialCartAnalysisState, action) => {
  switch (action.type) {
    case ActionTypes.GET_ANALYZED_CART:
      const datas = {
        ...state,
        isLoading: false,
        success: true,
        todaysCart: action.payload.todaysCart,
        todaysEarnings: action.payload.todaysEarnings,
        todaysCartItemCount: action.payload.todaysCartItemCount,
        listedCartCount: action.payload.listedCartCount

      };
      return datas;
    default:
      return state;
  }

};
