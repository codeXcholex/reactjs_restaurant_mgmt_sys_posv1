import * as ActionTypes from "./actionTypes";
import axios from "axios";

export const analyzedCartLoading = () => ({
  type: ActionTypes.ANALYZED_CART_LOADING,
});



export const getAnalyzedCartData = () => async (dispatch) => {
  try {
    dispatch(analyzedCartLoading());
    const BACKEND_BASE_URL = "https://surfit.onrender.com";
    const { data } = await axios.get(BACKEND_BASE_URL + "/api/cart/analysis");
    data.success && dispatch({ type: ActionTypes.GET_ANALYZED_CART, payload: data })
  } catch (error) {
    console.error("Error:", error);
  }
};

