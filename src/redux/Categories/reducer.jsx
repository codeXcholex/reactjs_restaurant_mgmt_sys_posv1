import * as ActionTypes from "./actionTypes"

const initialState = {
  categories: [],
  isLoading: true,
  success: null,
};

export const Categories = (state = initialState, action) => {
  switch (action.type) {
    case ActionTypes.GET_CATEGORIES:
      const datas = {
        ...state,
        categories: action.payload,
        isLoading: false,
        success: true
      };
      return datas;
    default:
      return state;
  }

};
