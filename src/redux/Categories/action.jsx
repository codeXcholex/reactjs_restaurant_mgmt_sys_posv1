import * as ActionTypes from "./actionTypes";
import axios from "axios";

export const categoryLoading = () => ({
  type: ActionTypes.CATEGORIES_LOADING,
});


export const getCategories = () => async (dispatch) => {
  try {
    dispatch(categoryLoading());
    const BACKEND_BASE_URL = "https://surfit.onrender.com";
    const { data } = await axios.get(BACKEND_BASE_URL + "/api/category/cats");
    data.success && dispatch({ type: ActionTypes.GET_CATEGORIES, payload: data.categories })
  } catch (error) {
    console.error("Error:", error);
  }
};

