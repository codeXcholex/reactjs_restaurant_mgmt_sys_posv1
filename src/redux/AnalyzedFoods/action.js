import * as ActionTypes from "./actionTypes";
import axios from "axios";

export const analyzedFoodLoading = () => ({
  type: ActionTypes.ANALYZED_FOOD_LOADING,
});



export const getAnalyzedFoodData = () => async (dispatch) => {
  try {
    dispatch(analyzedFoodLoading());
    const BACKEND_BASE_URL = "https://surfit.onrender.com";
    const { data } = await axios.get(BACKEND_BASE_URL + "/api/foods/analysis");
    console.log("pig");
    console.log(data);
    console.log("pig");
    data.success && dispatch({ type: ActionTypes.GET_ANALYZED_FOOD, payload: data })
  } catch (error) {
    console.error("Error:", error);
  }
};

