import * as ActionTypes from "./actionTypes";

const initialState = {
  bills: [],
  totalBills: "",
  currentPage: "",
  totalPages: "",
  pageSize: "",
  isLoading: true,
  success: null,
};

export const Bills = (state = initialState, action) => {
    console.log("pilload");
    console.log(action.type);
    console.log("pilload");
  switch (action.type) {
    case ActionTypes.GET_BILLS:
       
      const datas = {
        ...state,
        bills: action.payload.filteredCarts,
        totalBills: action.payload.totalBills,
        currentPage: action.payload.currentPage,
        totalPages: action.payload.totalPages,
        pageSize: action.payload.pageSize,
        isLoading: false,
        success: true
      };
      return datas;
    default:
      return state;
  }

};