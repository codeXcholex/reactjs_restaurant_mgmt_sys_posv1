import axios from "axios";

import * as ActionTypes from "./actionTypes"

export const billsLoading = () => ({
  type: ActionTypes.BILLS_LOADING
});
export const getBills = (page,sortQuery,customerId) => async (dispatch) => {
  try {
    if(customerId){
    dispatch(billsLoading());
    const BACKEND_BASE_URL = "https://surfit.onrender.com";
    const { data } = await axios.get(BACKEND_BASE_URL + `/api/cart/filters?page=${page || 1}&size=10&sortQuery=${sortQuery || "Latest"}&customer_id=${customerId}`);
    return data.success && dispatch({ type: ActionTypes.GET_BILLS, payload: data })}
    dispatch(billsLoading());
    const BACKEND_BASE_URL = "https://surfit.onrender.com";
    const { data } = await axios.get(BACKEND_BASE_URL + `/api/cart/filters?page=${page || 1}&size=10&sortQuery=${sortQuery || "Latest"}`);
    return data.success && dispatch({ type: ActionTypes.GET_BILLS, payload: data })
  } catch (error) {
    console.error("Error:", error);
  }
};