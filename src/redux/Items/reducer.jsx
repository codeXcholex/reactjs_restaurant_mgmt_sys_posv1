import * as ActionTypes from "./actionTypes";

const initialState = {
  Foods: [],
  totalFoods: "",
  currentPage: "",
  totalPages: "",
  pageSize: "",
  isLoading: true,
  success: null,
};

export const Items = (state = initialState, action) => {
  switch (action.type) {
    case ActionTypes.GET_ITEMS:

      const datas = {
        ...state,
        Foods: action.payload.filteredFoods,
        totalFoods: action.payload.totalFoods,
        currentPage: action.payload.currentPage,
        totalPages: action.payload.totalPages,
        pageSize: action.payload.pageSize,
        isLoading: false,
        success: true
      };
      return datas;
    default:
      return state;
  }

};