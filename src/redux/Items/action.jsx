import axios from "axios";

import * as ActionTypes from "./actionTypes"

export const itemLoading = () => ({
  type: ActionTypes.ITEMS_LOADING
});
export const getFoods = (categoryId, page, sortQuery) => async (dispatch) => {
  try {
    dispatch(itemLoading());
    const BACKEND_BASE_URL = "https://surfit.onrender.com";
    const { data } = await axios.get(BACKEND_BASE_URL + `/api/foods/filters?page=${page || 1}&size=10&sortQuery=${sortQuery}&category=${categoryId}`);
    data.success && dispatch({ type: ActionTypes.GET_ITEMS, payload: data })
  } catch (error) {
    console.error("Error:", error);
  }
};

export const itemLoadingFailed = (errorMessage) => ({
  type: ActionTypes.ITEM_LOADING_FAILED,
  payload: errorMessage,
});


export const fetchItemDetail = (_id) => async (dispatch) => {
  try {
    dispatch(itemLoading());
    const BACKEND_BASE_URL = "https://surfit.onrender.com";
    const { data } = await axios.get(BACKEND_BASE_URL + "/api/category");
    data.success && dispatch({
      type: ActionTypes.ALL_ITEMS_LOADING,
      payload: data.categories
    })
  }
  catch (err) {
    console.log("Error", err);
    return err;
  }
};

export const removeSelectedItemDetail = () => {
  return {
    type: ActionTypes.REMOVE_SELECTED_ITEM_DETAIL,
  }
}