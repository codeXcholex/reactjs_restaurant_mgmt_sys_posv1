import React, { useState, useEffect } from 'react';
import { useSelector } from 'react-redux';
import { ToastContainer, toast } from 'react-toastify';
import { FiMinus, FiPlus, FiX } from 'react-icons/fi';
import 'react-toastify/dist/ReactToastify.css';
import axios from 'axios';
import { getAnalyzedCartData } from '@/redux/AnalyzedCart/action';
import { useDispatch } from 'react-redux';


function Cart(props) {
    
const dispatch = useDispatch();
const [retrunAmount,setReturnAmount] = useState(0);
  const usrs = useSelector((state) => state.users);
  const [users, setUsers] = useState([]);
  
  const [prev, setPrev] = useState(props.selectedItem);
  const storedDataLocalStorage = localStorage.getItem("Customers");
  const Customers = JSON.parse(storedDataLocalStorage) || [];

  const [cartItems, setCartItems] = useState([]);

  const [showForm, setShowForm] = useState(false);
  const [formData, setFormData] = useState({
    customer_id: "",
    orders: [],
    payAmt:0
  });


  useEffect(() => {
    usrs.success && setUsers(usrs.Users)
  }, [])



  useEffect(() => {
    if (props.selectedItem) {
      const isItemAlreadyAdded = cartItems.find((cartItem) => cartItem._id === props.selectedItem._id);

      if (isItemAlreadyAdded) {
        toast.error(`${props.selectedItem.name} has already been added to the cart`, {
          toastId: 'alreadyAdded',
          className: 'custom-toast-error',
        });
      } else {
        const newItem = { ...props.selectedItem, quantity: 1, date: new Date().toLocaleDateString() };
        setCartItems((prevItems) => [...prevItems, newItem]);
        toast.success(`${newItem.name} has been successfully added to the cart`);
      }
    }
  }, [props.selectedItem]);

  const handleRemoveFromCart = (item) => {
    const updatedCartItems = cartItems.filter((cartItem) => cartItem._id !== item._id);
    setCartItems(updatedCartItems);
    toast.success(`${item.name} has been successfully removed from the cart`);
  };

  const handleIncreaseQuantity = (item) => {
    const updatedCartItems = cartItems.map((cartItem) => {
      if (cartItem._id === item._id) {
        return { ...cartItem, quantity: cartItem.quantity + 1 };
      }
      return cartItem;
    });
    setCartItems(updatedCartItems);
  };

  const calculateReturn = (Payable, Paid) =>{
    return Paid-Payable
  }

  const handleInputChange = (event) => {
    const { name, value } = event.target;
    setFormData((prevState) => ({
      ...prevState,
      [name]: value,
    }));
    setReturnAmount(calculateReturn(calculateTotalPrice(),formData.payAmt));
  };

  // const handleChange = (e) => {
  //   setFormValues({ ...formValues, [e.target.name]: e.target.value });
  // };

  const handleDecreaseQuantity = (item) => {
    const updatedCartItems = cartItems.map((cartItem) => {
      if (cartItem._id === item._id && cartItem.quantity > 1) {
        return { ...cartItem, quantity: cartItem.quantity - 1 };
      }
      return cartItem;
    });
    setCartItems(updatedCartItems);
  };

  const calculateSubtotal = () => {
    const subtotal = cartItems.reduce((total, item) => total + item.price * item.quantity, 0);
    return subtotal.toFixed(2);
  };

  const calculateTax = () => {
    const tax = calculateSubtotal() * 0.13;
    return tax.toFixed(2);
  };

  const calculateTotalPrice = () => {
    const total = parseFloat(calculateSubtotal()) + parseFloat(calculateTax());
    return total.toFixed(2);
  };

  const handleAddClick = () => {
    handleProceedOrders();
  };

  const handleProceedOrder = async (e) => {
    e.preventDefault();

    console.log("errs");
    console.log(formData);
    console.log("errs");
    console.log(cartItems);
    // Calculate the cart total
    const jsonPayload = calculateCartTotal(cartItems, formData.customer_id);
    console.log("json payload");
    console.log(jsonPayload);
    console.log("json payload");
    const response = await axios.post('https://surfit.onrender.com/api/cart/create', { ...jsonPayload, paidAmount: formData.payAmt }, {
      headers: {
        'Content-Type': 'application/json',
      },
    });
    toast.success(`${users.find((user) => user._id === formData.customer_id).fullName} has successfully proceeded ${cartItems.length} items through cart`);
    if (response.data.success) {
      dispatch(getAnalyzedCartData());
      setShowForm(false);
      setCartItems([])
      return setFormData({
        customer_id: "",
        orders: []
      });
    }
    return toast.success(`Error to proceed the orders`);
  };

  function calculateCartTotal(cartItems, customer_id) {
    let subtotal = 0;
    let taxAmount = 0;
    let totalPayable = 0;
    const items = [];

    // Calculate subtotal and populate items array
    cartItems.forEach((item) => {
      const { _id, quantity } = item;
      subtotal += item.price * quantity;
      items.push({ item_id: _id, quantity });
    });

    // Calculate tax amount (assuming 13% tax rate)
    taxAmount = subtotal * 0.13;

    // Calculate total payable
    totalPayable = subtotal + taxAmount;

    const jsonPayload = {
      customer_id,
      items,
      subTotal: subtotal,
      taxAmount,
      totalPayable,
    };

    return jsonPayload;
  }



  const handleProceedOrders = async (e) => {
    e.preventDefault();

    console.log("errs");
    console.log(formData);
    console.log("errs");
    console.log(cartItems);
    // Calculate the cart total
    const jsonPayload = calculateCartTotal(cartItems, formData.customer_id);
    console.log("json payload");
    console.log(jsonPayload);
    console.log("json payload");
    const response = await axios.post('https://surfit.onrender.com/api/cart/create', jsonPayload, {
      headers: {
        'Content-Type': 'application/json',
      },
    });
    toast.success(`${users.find((user) => user._id === formData.customer_id).fullName} has successfully proceeded ${cartItems.length} items through cart`);
    if (response.data.success) {
      dispatch(getAnalyzedCartData());
      setShowForm(false);
      setCartItems([])
      return setFormData({
        customer_id: "",
        orders: []
      });
    }
    return toast.success(`Error to proceed the orders`);
  };

  const isAuthenticated = () => {
    return true;
  };

  


  return (
    <div className="flex flex-col mt-8 ml-6 px-5">
      <h2 className="text-2xl font-bold mb-4">Cart</h2>
      {isAuthenticated() ? (
        <div>
          {cartItems.length === 0 ? (
            <p>Cart is empty</p>
          ) : (
            <>
              {cartItems.map((item) => (
                <div key={item._id} className="flex items-center justify-between mb-2">
                  <div className="flex flex-col items-center">
                    <img src={item.image} alt={item.name} style={{ width: '50px', height: '50px' }} /> {/* Display the small image */}
                    <div className="ml-4">
                      <h3>{item.name}</h3>
                      <p>{`$${item.price.toFixed(2)}`}</p>
                      <h3>Date: {item.date}</h3>
                    </div>
                    <div className="flex items-center">
                      <button className="text-blue-500 mx-1" onClick={() => handleDecreaseQuantity(item)}>
                        <FiMinus />
                      </button>
                      <p>{item.quantity}</p>
                      <button className="text-blue-500 mx-1" onClick={() => handleIncreaseQuantity(item)}>
                        <FiPlus />
                      </button>
                      <button className="text-red-500 mx-2" onClick={() => handleRemoveFromCart(item)}>
                        <FiX />
                      </button>
                    </div>
                  </div>
                </div>
              ))}
              <hr className="my-4" />
              <div className="flex items-center justify-between">
                <h3 className="font-bold">Subtotal:</h3>
                <p>{`$${calculateSubtotal()}`}</p>
              </div>
              <div className="flex items-center justify-between">
                <h3 className="font-bold">Tax (13%):</h3>
                <p>{`$${calculateTax()}`}</p>
              </div>
              <div className="flex items-center justify-between">
                <h3 className="font-bold">Total Payable:</h3>
                <p>{`$${calculateTotalPrice()}`}</p>
              </div>

              <div className="flex items-center justify-between">
                <label htmlFor="price" className="text-gray-700 font-semibold block mb-2">
              Paying Amt
            </label>
            <input
              type="number"
              step="0.01"
              id="payAmt"
              name="payAmt"
              value={formData.payAmt}
              onChange={handleInputChange}
              className="w-full px-3 py-2 border border-gray-300 rounded shadow-sm focus:outline-none focus:border-purple-500"
              required
            />
            </div>
            <div className="flex items-center justify-between">
                <h3 className="font-bold">Return Amount:</h3>
                <p>{retrunAmount}</p>
              </div>
              <div className="flex items-center justify-between">
              <label htmlFor="nameInput">Customer</label>
                      <select
                        id="nameInput"
                        name="customer_id"
                        value={formData.customer_id}
                        onChange={handleInputChange}
                        className="text-black bg-white"
                      >
                        <option value="">Select Customer</option>
                        {users.map((user) => (
                          <option key={user._id} value={user._id}>
                            {user.fullName}
                          </option>
                        ))}
                      </select>
                      </div>
              <div className="flex justify-center mt-4">
                <button className="px-4 py-2 mx-2 rounded bg-gray-200">Hold</button>
                <button className="px-4 py-2 mx-2 rounded bg-blue-500 text-white" onClick={handleProceedOrder}>Proceed</button>
                {showForm && (
                  <div className="fixed top-0 left-0 right-0 bottom-0 flex items-center justify-center bg-gray-500 bg-opacity-50">
                    <form onSubmit={handleProceedOrders} className="bg-white rounded p-6 shadow-md">


                      {/* Form fields */}
                      <div className="flex flex-row justify-end">
                        <button onClick={() => setShowForm(false)}>X</button>
                      </div>
                      <label htmlFor="nameInput">Customer</label>
                      <select
                        id="nameInput"
                        name="customer_id"
                        value={formData.customer_id}
                        onChange={handleInputChange}
                        className="text-black bg-white"
                      >
                        <option value="">Select Customer</option>
                        {users.map((user) => (
                          <option key={user._id} value={user._id}>
                            {user.fullName}
                          </option>
                        ))}
                      </select>

                      {/* Submit button */}
                      <div className="flex justify-end mt-4">
                      
                        <button type="submit" className="bg-blue-500 text-white py-2 px-4 rounded">
                          Proceed Orders
                        </button>
                      </div>
                    </form>
                  </div>
                )}
              </div>
            </>
          )}
        </div>
      ) : (
        <p>Please login to view the cart</p>
      )}
      <ToastContainer />
    </div>
  );
}

export default Cart;
