import React, { useState, useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { toast } from 'react-toastify';
// import { getFoods } from '@/redux/Items/action';
import axios from 'axios';
import { getUsers } from '@/redux/Users/action';

function UserForm({ editing, formData, onClose}) {
  const dispatch = useDispatch();
  const [formValues, setFormValues] = useState({
    fullName: '',
    email:"",
    phoneNumber: '',
    address: '',
    avatar: ''
  });

  useEffect(() => {
    if (editing) {
      setFormValues(formData);
    } else {
      setFormValues({
        fullName: '',
        email:"",
        phoneNumber: '',
        address: '',
        avatar: ''
      });
    }
  }, [editing, formData]);

  const handleSubmit = async (e) => {
    try {
      e.preventDefault();
      if (editing) {
        const response = await axios.patch(
          `https://surfit.onrender.com/api/pos/${formData._id}`,
          formValues,
          {
            headers: {
              'Content-Type': 'application/json',
            },
          }
        );
        if (response.data.success) {
        //   dispatch(getFoods(selectedCategory, page, sorting));
        dispatch(getUsers());
          toast.success(`User ${response.data.updatedUser.fullName} has been successfully updated.`);
          onClose();
        }
      } else {
        const response = await axios.post(
          'https://surfit.onrender.com/api/pos/create',
          formValues,
          {
            headers: {
              'Content-Type': 'application/json',
            },
          }
        );
        // dispatch(getFoods(selectedCategory, page, sorting));
        dispatch(getUsers());
        response.data.success && toast.success(`User ${response.data.savedUser.fullName} has been successfully added.`);
        onClose();
      }
    } catch (error) {
      console.log(error);
    }
  };

  const handleChange = (e) => {
    setFormValues({ ...formValues, [e.target.name]: e.target.value });
  };

  return (
    <div className="fixed inset-0 bg-gray-900 bg-opacity-50 flex justify-center items-center">
      <div className="bg-white w-96 p-4 rounded">
        <h2 className="text-xl font-bold mb-4">{editing ? 'Edit Food' : 'Add Food'}</h2>
        <form onSubmit={handleSubmit}>
          <div className="mb-4">
            <label htmlFor="name" className="text-gray-700 font-semibold block mb-2">
              Name
            </label>
            <input
              type="text"
              id="fullName"
              name="fullName"
              value={formValues.fullName}
              onChange={handleChange}
              className="w-full px-3 py-2 border border-gray-300 rounded shadow-sm focus:outline-none focus:border-purple-500"
              required
            />
          </div>
          <div className="mb-4">
            <label htmlFor="description" className="text-gray-700 font-semibold block mb-2">
              Address
            </label>
            <input
              type="text"
              id="address"
              name="address"
              value={formValues.address}
              onChange={handleChange}
              className="w-full px-3 py-2 border border-gray-300 rounded shadow-sm focus:outline-none focus:border-purple-500"
              required
            />
          </div>
          <div className="mb-4">
            <label htmlFor="price" className="text-gray-700 font-semibold block mb-2">
              Contact
            </label>
            <input
              type="number"
              id="phoneNumber"
              name="phoneNumber"
              value={formValues.phoneNumber}
              onChange={handleChange}
              className="w-full px-3 py-2 border border-gray-300 rounded shadow-sm focus:outline-none focus:border-purple-500"
              required
            />
          </div>
          <div className="mb-4">
            <label htmlFor="price" className="text-gray-700 font-semibold block mb-2">
              Email
            </label>
            <input
              type="text"
              id="email"
              name="email"
              value={formValues.email}
              onChange={handleChange}
              className="w-full px-3 py-2 border border-gray-300 rounded shadow-sm focus:outline-none focus:border-purple-500"
              required
            />
          </div>
          <div className="mb-4">
            <label htmlFor="image" className="text-gray-700 font-semibold block mb-2">
              Image
            </label>
            <input
              type="text"
              id="avatar"
              name="avatar"
              value={formValues.avatar}
              onChange={handleChange}
              className="w-full px-3 py-2 border border-gray-300 rounded shadow-sm focus:outline-none focus:border-purple-500"
              required
            />
          </div>
          <div className="flex justify-end">
            <button
              type="submit"
              className="bg-purple-500 hover:bg-purple-600 px-4 py-2 rounded text-white mr-2"
            >
              {editing ? 'Update' : 'Add'}
            </button>
            <button
              type="button"
              className="bg-gray-300 hover:bg-gray-400 px-4 py-2 rounded text-gray-700"
              onClick={onClose}
            >
              Cancel
            </button>
          </div>
        </form>
      </div>
    </div>
  );
}

export default UserForm;
