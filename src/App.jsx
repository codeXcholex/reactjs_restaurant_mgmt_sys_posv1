import { Routes, Route, Navigate } from "react-router-dom";
import { Dashboard, Auth } from "@/layouts";
import React, { useEffect } from "react";
import { useDispatch } from "react-redux";
import { getCategories } from "./redux/Categories/action";
import { getUsers } from "./redux/Users/action";
import { getAnalyzedFoodData } from "./redux/AnalyzedFoods/action";
import { getAnalyzedCartData } from "./redux/AnalyzedCart/action";
import { getBills } from "./redux/Orders/action";

function App() {

  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(getCategories());
    dispatch(getUsers());
    dispatch(getBills());
    dispatch(getAnalyzedCartData());
    dispatch(getAnalyzedFoodData());
    
    // dispatch(getFoods("",1,""));
  }, [dispatch]);
  return (
    <Routes>
      <Route path="/dashboard/*" element={<Dashboard />} />
      <Route path="/auth/*" element={<Auth />} />
      <Route path="*" element={<Navigate to="/dashboard/home" replace />} />
    </Routes>
  );
}

export default App;
