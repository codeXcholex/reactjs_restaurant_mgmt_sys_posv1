import React, { useState, useEffect } from 'react';
import { useSelector, useDispatch } from "react-redux";
import Cart from '@/widgets/cards/cart';
import { FiShoppingCart } from 'react-icons/fi';
import { getFoods } from '@/redux/Items/action';
import { getAnalyzedCartData } from '@/redux/AnalyzedCart/action';

function Main_menu() {
  const dispatch = useDispatch();

  const [items, setItems] = useState([]);
  const [categories, setCategories] = useState([]);
  const [selectedCategory, setSelectedCategory] = useState("6485ea1c8b304e088869e84b");

  const [searchTerm, setSearchTerm] = useState("");
  const [showCart, setShowCart] = useState(false);
  const [selectedItem, setSelectedItem] = useState(null);

  const cats = useSelector((state) => state.category);
  const foods = useSelector((state) => state.foods);



  useEffect(() => {
    if (cats.success) {
      setCategories(cats.categories);
    }
  }, [cats]);

  useEffect(() => {
    foods.success && setItems(foods.Foods)
  }, [foods]);

  useEffect(() => {
    dispatch(getFoods(selectedCategory));
  }, [])

  useEffect(() => {
    dispatch(getFoods(selectedCategory))
  }, [selectedCategory])

  const handleSearch = (term) => {
    setSearchTerm(term);
  };

  const handleCategoryChange = (category) => {
    setSelectedCategory(category);
  };

  const handleAddToCart = (item) => {
    setSelectedItem(item);
    setShowCart(true);
  };

  const handleToggleCart = () => {
    setShowCart(!showCart);
  };
  

  return (
    <>
      <div className="flex justify-center">
      <div className="w-full md:w-9/12">
        {/* Category search bar, Switch bar & items */}
        <div className="flex flex-col justify-evenly items-center">
          {/* Search bar */}
          <div className="relative mx-2 my-4 bg-purple-200">
            <input
              type="text"
              placeholder="Search items..."
              className="w-64 px-4 py-2 pr-10 rounded border border-gray-300 focus:outline-none focus:border-blue-500"
              onChange={(e) => handleSearch(e.target.value)}
            />
            <span className="absolute top-2 right-14 text-gray-500">
              <svg
                xmlns="http://www.w3.org/2000/svg"
                className="h-5 w-5"
                viewBox="0 0 20 20"
                fill="currentColor"
              >
                <path
                  fillRule="evenodd"
                  d="M9.5 2a7.5 7.5 0 015.875 12.121l4.614 4.615a1 1 0 11-1.414 1.414l-4.615-4.614A7.5 7.5 0 119.5 2zm0 2a5.5 5.5 0 100 11 5.5 5.5 0 000-11z"
                  clipRule="evenodd"
                />
              </svg>
            </span>
          </div>

          {/* Cart icon */}
          <div className={`absolute top-8 right-5 text-2xl text-blue-500 ${showCart ? 'z-10' : ''}`}>
  <FiShoppingCart
    onClick={handleToggleCart}
    className="transition-transform duration-300 transform hover:scale-110"
    style={{ filter: 'drop-shadow(0px 4px 4px rgba(0, 0, 0, 0.25))' }}
  />
</div>


          {/* Category switch bar */}
          <div className="flex justify-center mt-8 overflow-x-auto">
            {categories.map((category) => (
              <button
                key={category._id}
                className={`px-2 md:px-4 py-2 mx-1 md:mx-2 rounded ${selectedCategory === category._id ? 'bg-blue-500 text-white' : 'bg-gray-200'
                  }`}
                onClick={() => handleCategoryChange(category._id)}
              >
                <span className=" md:inline">{category.name}</span>
              </button>
            ))}
          </div>


          {/* Food item listing */}
          <div className="grid grid-cols-3 gap-4 mt-8">
            {items
              .filter((item) => item.name.toLowerCase().includes(searchTerm.toLowerCase()) && item.category === selectedCategory)
              .map((item) => (
                <div
                  key={item._id}
                  className="flex flex-col items-center justify-center p-4 bg-white border border-gray-300 rounded shadow"
                >
                  <img src={item.image} alt={item.name} className="w-24 h-24 object-cover mb-2 rounded" />
                  <h3 className="text-xl font-semibold">{item.name}</h3>
                  <p className="text-gray-500">${item.price.toFixed(2)}</p>
                  <button
                    className="px-4 py-2 mt-4 bg-blue-500 text-white rounded"
                    onClick={() => handleAddToCart(item)}
                  >
                    Add to Cart
                  </button>
                </div>
              ))}
          </div>
        </div>
      </div>

      {/* Cart */}
      <div className={`fixed top-0 right-0 h-screen w-1/7 bg-black bg-opacity-50 transform ${showCart ? 'translate-x-0' : 'translate-x-full'
        } transition-transform duration-300 ease-in-out`}>
        <div className="bg-white h-full p-4">
          <Cart selectedItem={selectedItem} />
        </div>
      </div>
      </div>
    </>
  );
}

export default Main_menu;
