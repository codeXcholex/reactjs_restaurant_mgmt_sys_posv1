import React, { useState, useEffect } from 'react';
import { useSelector, useDispatch } from "react-redux";
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { FiShoppingCart } from 'react-icons/fi';
import FoodForm from '@/widgets/forms/foodForm';
import { getFoods } from '@/redux/Items/action';
import sortFlowsHome from '@/assets/sortFlowHome';
import { FaPlus } from 'react-icons/fa';
import axios from 'axios';
import {
  Typography,
} from "@material-tailwind/react";

import { StatisticsCard } from "@/widgets/cards";

import { GetCartDynamicData } from '@/assets/analysisData';
import { getAnalyzedFoodData } from '@/redux/AnalyzedFoods/action';

export function Home() {

  const dispatch = useDispatch();
  const [items, setItems] = useState([]);
  const [categories, setCategories] = useState([]);
  const [searchTerm, setSearchTerm] = useState("");
  // dispatch(getAnalyzedFoodData());

  
  // console.log(DynamicCartDetail);


  const [formData, setFormData] = useState({
    _id: "",
    name: '',
    description: '',
    price: '',
    image: '',
    category: ''
  });


  const [editing, setEditing] = useState(false);
  const [showForm, setShowForm] = useState(false);
  const analyzedFoods = useSelector((state) => state.analyzedFood);
  const cats = useSelector((state) => state.category);
  const foods = useSelector((state) => state.foods);
  const [food,setFood] = useState([])

  // let DynamicCartDetail =[];
  const [dynamicCartDetail,setDynamicCartDetail] = useState([]);

  useEffect(() => {
    if(analyzedFoods.success)
     {
      setDynamicCartDetail(GetCartDynamicData("Foods", analyzedFoods ))
  }})

  // const DynamicCartDetail = GetCartDynamicData("Foods",food )



  const [page, setPage] = useState(1);
  const [sorting, setSorting] = useState("A-Z");
  const [selectedCategory, setSelectedCategory] = useState("");


  useEffect(() => {
    dispatch(getFoods(selectedCategory, page, sorting));
  }, [sorting, page, selectedCategory])


  useEffect(() => {
    foods.success && setItems(foods.Foods)
  }, [foods])



  useEffect(() => {
    if (cats.success) {
      setCategories(cats.categories);
    }
  }, [cats]);


  useEffect(() => {
    setCategories(cats.categories);
  }, [categories]);


  const handleAdd = () => {
    setEditing(false);
    setShowForm(true);

  };

  const handleFormClose = () => {
    setShowForm(false);
    dispatch(getFoods(selectedCategory, page, sorting));
  };


  const handleEdit = (foodId) => {
    const editingFood = foods.Foods.find((food) => food._id === foodId);
    setFormData(editingFood);
    setEditing(true);
    setShowForm(true);

  };


  const handleDelete = async (_id, name) => {
    try {
      const response = await axios.delete(
        `https://surfit.onrender.com/api/foods/${_id}`,
        {
          headers: {
            'Content-Type': 'application/json',
          },
        }
      );
      setFormData({
        _id: "",
        name: '',
        description: '',
        price: '',
        image: '',
        category: ''
      });
      console.log("Deleting");
      console.log("Deleting");
      dispatch(getFoods(selectedCategory, page, sorting));
      response.data.success && toast.success(`Item ${name} has been successfully deleted.`);
    }
    catch (err) {
      console.log(err);
    }
  };


  const handlePageChange = (page) => {
    setPage(page);
    console.log('Page changed to', page);
  };


  const handleSortBy = (value) => {
    setSorting(value);
  }


  const handleCategoryChange = (categoryId) => {
    setSelectedCategory(categoryId);
  }



  return (
    <div className="mt-12">
      <ToastContainer />
      {/* Searchbar */}
      <div className="flex items-center justify-center mx-2 my-6">
        <div className="relative">
          <input
            type="text"
            placeholder="Search items..."
            className="w-full md:w-64 px-4 py-2 pr-10 rounded border border-gray-300 focus:outline-none focus:border-blue-500"
            onChange={(e) => handleSearch(e.target.value)}
          />
          <span className="absolute top-2 right-2 text-gray-500">
            <svg
              xmlns="http://www.w3.org/2000/svg"
              className="h-5 w-5"
              viewBox="0 0 20 20"
              fill="currentColor"
            >
              <path
                fillRule="evenodd"
                d="M9.5 2a7.5 7.5 0 015.875 12.121l4.614 4.615a1 1 0 11-1.414 1.414l-4.615-4.614A7.5 7.5 0 119.5 2zm0 2a5.5 5.5 0 100 11 5.5 5.5 0 000-11z"
                clipRule="evenodd"
              />
            </svg>
          </span>
        </div>
      </div>

      {/* Analysis */}
      <div className="mb-12 grid gap-y-10 gap-x-6 md:grid-cols-2 xl:grid-cols-4">
        {dynamicCartDetail.map(({ icon, title, footer, ...rest }) => (
          <StatisticsCard
            key={title}
            {...rest}
            title={title}
            icon={React.createElement(icon, {
              className: "w-6 h-6 text-white",
            })}
            footer={
              <Typography className="font-normal text-blue-gray-600">
                <strong className={footer.color}>{footer.value}</strong>
                &nbsp;{footer.label}
              </Typography>
            }
          />
        ))}
      </div>
      {/* sort & category select */}
      <div className="flex justify-between items-center mb-4">
        <div>
          <span className="text-lg font-semibold">Sort By:</span>
          <select
            className="bg-white border border-gray-300 rounded px-3 py-1 ml-2"
            onChange={(e) => handleSortBy(e.target.value)}
          >
            {sortFlowsHome.map((item) => (
              <option key={item._id} value={item._id}>
                {item.flow}
              </option>
            ))}
          </select>
        </div>
        <div>
          <span className="text-lg font-semibold">Select Category</span>
          <select
            className="bg-white border border-gray-300 rounded px-3 py-1 ml-2"
            onChange={(e) => handleCategoryChange(e.target.value)}
          >
            <option value="">All</option>
            {categories.map((category) => (
              <option key={category._id} value={category._id}>
                {category.name}
              </option>
            ))}
          </select>
        </div>
      </div>
      {/* Table */}
      <div className="mt-8">
        <table className="w-full border">
          {/* Table headers */}
          <thead>
            <tr>
              <th className="border px-4 py-2 text-left">Name</th>
              <th className="border px-4 py-2 text-left">Description</th>
              <th className="border px-4 py-2 text-left">Price</th>
              <th className="border px-4 py-2 text-left">Category</th>
              <th className="border px-4 py-2 text-left">Image</th>
              <th className="border px-4 py-2 text-left">Orders</th>
              <th className="border px-4 py-2 text-left">Actions</th>
            </tr>
          </thead>
          {/* Table body */}
          <tbody>
            {foods.Foods.map((item) => (
              <tr key={item._id}>
                <td className="border px-4 py-2">{item.name}</td>
                <td className="border px-4 py-2">{item.description}</td>
                <td className="border px-4 py-2">${item.price.toFixed(2)}</td>
                <td className="border px-4 py-2">{item.Category[0].name}</td>
                <td className="border px-4 py-2">
                  <img src={item.image} alt={item.name} className="w-10 h-10" />
                </td>
                <td className="border px-4 py-2">{item.orderCount}</td>
                <td className="border px-4 py-2">
                  <button
                    className="bg-blue-500 hover:bg-blue-600 px-2 py-1 rounded text-white mr-2"
                    onClick={() => handleEdit(item._id)}
                  >
                    Edit
                  </button>
                  <button
                    className="bg-red-500 hover:bg-red-600 px-2 py-1 rounded text-white"
                    onClick={() => handleDelete(item._id, item.name)}
                  >
                    Delete
                  </button>
                </td>
              </tr>
            ))}
          </tbody>
        </table>


        {showForm && (
          <FoodForm
            categories={categories}
            editing={editing}
            formData={formData}
            onClose={handleFormClose}
            selectedCategory={selectedCategory}
            page={page}
            sorting={sorting}
          />
        )}

        <div className="flex justify-center mt-4">
          {Array.from({ length: foods.totalPages }, (_, index) => (
            <button
              key={index}
              className={`bg-${index + 1 === Number(page) ? 'purple-600' : 'white'} hover:bg-green-600 transition-colors duration-300 ease-in-out px-4 py-2 rounded-full text-black mx-1`}
              onClick={() => handlePageChange(index + 1)}
            >
              {index + 1}
            </button>
          ))}
        </div>

        <button
          className="fixed bottom-10 right-15 bg-purple-500 hover:bg-purple-600 px-4 py-2 rounded-full text-white"
          onClick={handleAdd}
        >
          <FaPlus size={20} />
        </button>
      </div>
    </div>
  );


}

export default Home;
