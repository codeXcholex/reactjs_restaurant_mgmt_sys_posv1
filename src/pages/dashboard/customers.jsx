import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { useSelector } from "react-redux";
import { Link } from 'react-router-dom';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { useDispatch } from 'react-redux';
import { FaPlus } from 'react-icons/fa';
import UserForm from '@/widgets/forms/customerForm';
import { getUsers } from '@/redux/Users/action';
function Customers() {


  const dispatch = useDispatch();
  const [users, setUsers] = useState([]);


  const [formData, setFormData] = useState({
    _id: "",
    fullName: '',
    phoneNumber: '',
    email: '',
    avatar: '',
    address: ''
  });


  const [editing, setEditing] = useState(false);
  const [showForm, setShowForm] = useState(false);

  const usrs = useSelector((state) => state.users);



  const [page, setPage] = useState(1);
  const [sorting, setSorting] = useState("A-Z");


  useEffect(() => {
    usrs.success && setUsers(usrs.users)
  }, [usrs])



  const handleAdd = () => {
    setEditing(false);
    setShowForm(true);
  };

  const handleFormClose = () => {
    setShowForm(false);
  };


  const handleEdit = (userId) => {
    const editingUser = usrs.Users.find((user) => user._id === userId);
    setFormData(editingUser);
    setEditing(true);
    setShowForm(true);
  };


  const handleDelete = async (_id, name) => {
    try {
      const response = await axios.delete(
        `https://surfit.onrender.com/api/pos/${_id}`,
        {
          headers: {
            'Content-Type': 'application/json',
          },
        }
      );
      setFormData({
        _id: "",
        fullName: '',
        phoneNumber: '',
        email: '',
        avatar: '',
        address: ''
      });
      dispatch(getUsers());
      response.data.success && toast.success(`User ${name} has been successfully deleted.`);
    }
    catch (err) {
      console.log(err);
    }
  };



  return (
    <>
      <div className="mt-12">
        <ToastContainer />



        <div className="mt-8">
          <table className="w-full border">
            
            
            <thead>
              <tr>
                <th className="border px-4 py-2 text-left">Name</th>
                <th className="border px-4 py-2 text-left">Address</th>
                <th className="border px-4 py-2 text-left">Contact</th>
                <th className="border px-4 py-2 text-left">Email</th>
                <th className="border px-4 py-2 text-center">Avatar</th>
                <th className="border px-4 py-2 text-center">Actions</th>
              </tr>
            </thead>

           
            <tbody>
              {usrs.Users.map((user) => (
                <tr key={user._id}>
                  <td className="border px-4 py-2">{user.fullName}</td>
                  <td className="border px-4 py-2">{user.address}</td>
                  <td className="border px-4 py-2">${user.phoneNumber}</td>
                  <td className="border px-4 py-2">{user.email}</td>
                  <td className="border px-4 py-2 text-center">
                    <div className="flex justify-center items-center">
                      <div className="w-10 h-10 rounded-full overflow-hidden border-2 border-gray-300">
                        <img src={user.avatar} alt={user.name} className="w-full h-full object-cover" />
                      </div>
                    </div>
                  </td>
                  <td className="border px-4 py-2 flex justify-center">
                    <button
                      className="bg-green-500 hover:bg-green-600 px-2 py-1 rounded text-white mr-2"
                      onClick={() => handleEdit(user._id)}
                      style={{ border: 'none', outline: 'none' }}
                    >
                      Edit
                    </button>
                    <button
                      className="bg-orange-500 hover:bg-orange-600 px-2 py-1 rounded text-white"
                      onClick={() => handleDelete(user._id, user.name)}
                      style={{ border: 'none', outline: 'none' }}
                    >
                      Delete
                    </button>
                  </td>
                </tr>
              ))}
            </tbody>
          </table>


          {showForm && (
            <UserForm
              editing={editing}
              formData={formData}
              onClose={handleFormClose}
            />
          )}
          <button
            className="fixed bottom-10 right-15 bg-purple-500 hover:bg-purple-600 px-4 py-2 rounded-full text-white"
            onClick={handleAdd}
          >
            <FaPlus size={20} />
          </button>
        </div>
      </div>
    </>
  );
}

export default Customers;
