import React, { useState, useEffect } from "react";
import {
  Typography,
} from "@material-tailwind/react";

import { StatisticsCard } from "@/widgets/cards";
import { useSelector, useDispatch } from "react-redux";
import { getAnalyzedCartData } from "@/redux/AnalyzedCart/action";
import { getBills } from "@/redux/Orders/action";
import { GetCartDynamicData } from "@/assets/analysisData";
import sortFlowsBills from "@/assets/sortFlowBills";


export function Orders() {
  const dispatch = useDispatch();
  const [page, setPage] = useState(1);
  const [sorting, setSorting] = useState("Latest");
 

  const analyzedOrders = useSelector((state) => state.analyzedCart);
  const analyzedFoods = useSelector((state) => state.analyzedFood);
  const bills = useSelector((state) => state.bills);
  const usrs = useSelector((state) => state.users);

  const [searchTerm, setSearchTerm] = useState("");
  const [searchOptions, setSearchOptions] = useState([]);
  const [customer_id, setCustomer_id] = useState("");
  

  const DynamicCartDetail = GetCartDynamicData("Orders",analyzedOrders);

console.log("AFFF");
console.log(analyzedFoods);
console.log("AFFF");


  useEffect(() => {
    dispatch(getBills(page,sorting,customer_id));
  }, [sorting, page, customer_id])


  useEffect(() => {
    if (searchTerm === "") {
      setSearchOptions([]); // Clear the search options list
      return;
    }
  
    const filteredOptions = usrs.Users.filter(
      (user) =>
        user.phoneNumber.toString().includes(searchTerm) ||
        user.fullName.toLowerCase().includes(searchTerm.toLowerCase()) ||
        user._id.toLowerCase().includes(searchTerm.toLocaleLowerCase())
    );
  
    setSearchOptions(filteredOptions);
  }, [searchTerm]);


  const handlePageChange = (page) => {
    setPage(page);
  };


  const handleSortBy = (value) => {
    setSorting(value);
  }

  const handleInputChange = (event) => {
    setSearchTerm(event.target.value);
    if(event.target.value.length==0){
      setSearchOptions([]);
    }
  };

  const handleOptionClick = (user) => {
    setCustomer_id(user._id);
    setSearchTerm("");
    setSearchOptions([]);
  };

  return (
    <div className="mt-12">
      <div className="mb-12 grid gap-y-10 gap-x-6 md:grid-cols-2 xl:grid-cols-4">
        {DynamicCartDetail.map(({ icon, title, footer, ...rest }) => (
          <StatisticsCard
            key={title}
            {...rest}
            title={title}
            icon={React.createElement(icon, {
              className: "w-6 h-6 text-white",
            })}
            footer={
              <Typography className="font-normal text-blue-gray-600">
                <strong className={footer.color}>{footer.value}</strong>
                &nbsp;{footer.label}
              </Typography>
            }
          />
        ))}
      </div>

      {/* searchbar */}
      <div className="flex justify-center items-center">
      <div className="flex items-center justify-between">
        <input
          type="text"
          placeholder="Search by Name/Phone"
          value={searchTerm}
          onChange={handleInputChange}
          className="border border-gray-300 rounded-lg py-2 px-4 w-72"
        />
        {searchOptions.length > 0 && (
          <ul className="mt-2 w-72 bg-white border border-gray-300 rounded-lg max-h-48 overflow-y-auto">
            {searchOptions.map((user) => (
              <li
                key={user._id}
                onClick={() => handleOptionClick(user)}
                className="px-4 py-2 cursor-pointer hover:bg-gray-100"
              >
                {user.fullName}
              </li>
            ))}
          </ul>
        )}
      </div>
    </div>


            {/* Sort */}
      <div className="mt-8">
            <div>
              <span className="text-lg font-semibold">Sort By:</span>
              <select
                className="bg-white border border-gray-300 rounded px-3 py-1 ml-2"
                onChange={(e) => handleSortBy(e.target.value)}
              >
                 {sortFlowsBills.map((item) => (
                  <option key={item._id} value={item._id}>
                    {item.flow}
                  </option>
                ))}
              </select>
            </div>
            {/* 
             */}
      </div>


      <div className="mt-8">
        <table className="w-full border">
          {/* Table headers */}
          <thead>
            <tr>
              <th className="border px-4 py-2 text-left">Customer Name</th>
              <th className="border px-4 py-2 text-left">Bill Number</th>
              <th className="border px-4 py-2 text-left">Product Details</th>
              <th className="border px-4 py-2 text-left">Total</th>
              <th className="border px-4 py-2 text-left">Tax(13%)</th>
              <th className="border px-4 py-2 text-left">Payable</th>
              <th className="border px-4 py-2 text-left">Paid/Returned</th>
              <th className="border px-4 py-2 text-left">Checkout</th>
            </tr>
          </thead>
          {/* Table body */}
          <tbody>
            {bills.bills.map((item) => (
              <tr key={item._id}>
                <td className="border px-4 py-2">{item.user_id[0].fullName}</td>
                <td className="border px-4 py-2">{item._id}</td>
                <td className="border px-4 py-2">
                  {item.item.map((product) => (
                    <div key={product.id}>
                      <p>{product.name} * {product.quantity.quantity.toFixed(2)}</p>
                      <p>Price: ${product.price.toFixed(2)}</p>
                    </div>
                  ))}
                </td>
                <td className="border px-4 py-2">${item.subTotal.toFixed(2)}</td>
                <td className="border px-4 py-2">${item.taxAmount.toFixed(2)}</td>
                <td className="border px-4 py-2">${item.totalPayable.toFixed(2)}</td>
                <td className="border px-4 py-2">
                  <div>
                    <p>Paid: ${item.paidAmount}</p>
                    <p>Returned: ${item.returnAmount}</p>
                  </div>
                </td>
                <td className="border px-4 py-2">
                  {item.minutesAgo > 60
                    ? `${(item.minutesAgo / 60).toFixed(1)} hours ago`
                    : `${item.minutesAgo.toFixed(0)} min ago`}
                </td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
      <div className="flex justify-center mt-4">
        {Array.from({ length: bills.totalPages }, (_, index) => (
          <button
            key={index}
            className={`bg-${index + 1 === Number(page) ? 'purple-600' : 'white'} hover:bg-green-600 transition-colors duration-300 ease-in-out px-4 py-2 rounded-full text-black mx-1`}
            onClick={() => handlePageChange(index + 1)}
          >
            {index + 1}
          </button>
        ))}
      </div>
    </div>
  );


}

export default Orders;
